package com.example.photosandvideosapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.photosandvideosapp.R
import com.example.photosandvideosapp.application.constants.BundleConstants
import com.example.photosandvideosapp.databinding.FragmentPhotoListBinding
import com.example.photosandvideosapp.databinding.FragmentVideoListBinding
import com.example.photosandvideosapp.view.adapter.PhotoListRecyclerAdapter
import com.example.photosandvideosapp.view.adapter.VideoListAdapter


/**
 * Created by Surabhi on 13,February,2021
 */

class VideosListFragment :Fragment() {

    private lateinit var binding: FragmentVideoListBinding
    private var thumbnailUrlList:ArrayList<String> = ArrayList()
    private var photographerNameList:ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            thumbnailUrlList = it.getSerializable(BundleConstants.BUNDLE_THUMBNAIL_URL) as ArrayList<String>
            photographerNameList = it.getSerializable(BundleConstants.BUNDLE_PHOTOGRAPHER_NAME) as ArrayList<String>
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_video_list, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.fragment = this
        binding.videoListRecyclerview.adapter = VideoListAdapter(thumbnailUrlList,photographerNameList)

    }
}