package com.example.photosandvideosapp.view.viewholder

import android.content.Context
import com.example.photosandvideosapp.R
import com.example.photosandvideosapp.databinding.ItemPhotoListBinding
import com.example.photosandvideosapp.utils.GlideImageUtil
import com.example.photosandvideosapp.view.base.BaseViewHolder
import com.example.photosandvideosapp.view.listener.PhotoClickListener


/**
 * Created by Surabhi on 13,February,2021
 */

class PhotoListViewHolder(
    private val binding: ItemPhotoListBinding,
    private val context: Context,
    private val photoUrlList: ArrayList<String>,
    private val photographerNameList: ArrayList<String>,
    private val photoClickListener: PhotoClickListener
) : BaseViewHolder<Any>(binding.root){


    override fun bind(item: Any?, position: Int) {
        binding.viewHolder = this

        GlideImageUtil.loadImage(
            context = context,
            imageUrl = photoUrlList[position],
            icPlaceholder = R.drawable.ic_launcher_background,
            icError = R.drawable.ic_launcher_background,
            imageView = binding.image
        )

        binding.photographerName.text = photographerNameList[position]
    }

    fun onPhotoClick(){
        photoClickListener.onPhotoClick(photoUrlList[position], photographerNameList[position])
    }
}