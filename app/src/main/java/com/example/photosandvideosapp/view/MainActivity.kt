package com.example.photosandvideosapp.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.photosandvideosapp.R
import com.example.photosandvideosapp.databinding.ActivityMainBinding
import com.example.photosandvideosapp.utils.GlideImageUtil
import com.example.photosandvideosapp.view.adapter.PhotosVideosPagerAdapter
import com.example.photosandvideosapp.viewmodel.ui.MainActivityViewModel
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var viewModel: MainActivityViewModel? = null
    private val photoUrlList: ArrayList<String> = ArrayList()
    private val photographerNameList: ArrayList<String> = ArrayList()
    private val thumbnailUrlList: ArrayList<String> = ArrayList()
    private val videographerNameList: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.activity = this
        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)

        viewModel?.getHomeBannerImage()


        binding.tabPhotosVideos.addTab(
            binding.tabPhotosVideos.newTab().setText(getString(R.string.photos))
        )
        binding.tabPhotosVideos.addTab(
            binding.tabPhotosVideos.newTab().setText(getString(R.string.videos))
        )

        binding.tabPhotosVideos.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabUnselected(tab: TabLayout.Tab) {
                // no action
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                // no action
            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                binding.viewpagerPhotosVideos.currentItem = tab.position
            }
        })

        viewModel?.fetchPhotosList()
        viewModel?.fetchVideosList()
        observeViewModelData()
    }

    private fun observeViewModelData() {
        viewModel?.fetchCuratedPhotoResponse?.observe(
            this, { response ->
                GlideImageUtil.loadImage(
                    context = applicationContext,
                    imageUrl = response.photos[0].src.original,
                    icPlaceholder = R.drawable.ic_launcher_background,
                    icError = R.drawable.ic_launcher_background,
                    imageView = binding.imgHomeBanner
                )
            }
        )

        viewModel?.fetchPhotoListResponse?.observe(
            this, { response ->
                for (photoUrl in response?.photos!!) {
                    photoUrlList.add(photoUrl.src.original)
                }
                for (photoUrl in response.photos) {
                    photographerNameList.add(photoUrl.photographer)
                }
            }
        )

        viewModel?.fetchVideoListResponse?.observe(
            this, { response ->
                for (photoUrl in response?.videos!!) {
                    thumbnailUrlList.add(photoUrl.image)
                }
                for (photoUrl in response.videos) {
                    videographerNameList.add(photoUrl.user.name)
                }

                setPagerAdapter()
            }
        )

    }

    private fun setPagerAdapter() {
        val pagerAdapter = PhotosVideosPagerAdapter(
            supportFragmentManager,
            photoUrlList,
            photographerNameList,
            thumbnailUrlList,
            videographerNameList
        )
        binding.viewpagerPhotosVideos.adapter = pagerAdapter
    }
}