package com.example.photosandvideosapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.photosandvideosapp.R
import com.example.photosandvideosapp.application.constants.BundleConstants
import com.example.photosandvideosapp.databinding.FragmentPhotoDetailBinding
import com.example.photosandvideosapp.utils.GlideImageUtil


/**
 * Created by Surabhi on 13,February,2021
 */

class PhotoDetailFragment : Fragment() {

    private lateinit var binding: FragmentPhotoDetailBinding
    private var photographerName:String? =null
    private var photoUrl:String? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            photoUrl = it.getString(BundleConstants.BUNDLE_PHOTO_URL)
            photographerName = it.getString(BundleConstants.BUNDLE_PHOTOGRAPHER_NAME)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_photo_detail, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.fragment = this

        GlideImageUtil.loadImage(
            context = context,
            imageUrl = photoUrl,
            icPlaceholder = R.drawable.ic_launcher_background,
            icError = R.drawable.ic_launcher_background,
            imageView = binding.fullImage
        )
        binding.photographerName.text =photographerName
    }
}