package com.example.photosandvideosapp.view.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.photosandvideosapp.application.constants.AppConstants
import com.example.photosandvideosapp.application.constants.BundleConstants
import com.example.photosandvideosapp.view.fragments.PhotoListFragment
import com.example.photosandvideosapp.view.fragments.VideosListFragment


/**
 * Created by Surabhi on 13,February,2021
 */

class PhotosVideosPagerAdapter(
    supportFragmentManager: FragmentManager,
    private val photoUrlList: ArrayList<String>,
    private val photographerNameList: ArrayList<String>,
    private val thumbnailUrlList: ArrayList<String>,
    private val videographerNameList: ArrayList<String>
) : FragmentStatePagerAdapter(supportFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount(): Int = AppConstants.CONSTANT_2

    override fun getItem(position: Int): Fragment {
        return if (position == AppConstants.CONSTANT_0) {
            val fragment = PhotoListFragment()
            val bundle = Bundle()
            bundle.putSerializable(
                BundleConstants.BUNDLE_PHOTO_URL,
                photoUrlList
            )
            bundle.putSerializable(
                BundleConstants.BUNDLE_PHOTOGRAPHER_NAME,
                photographerNameList
            )
            fragment.arguments = bundle
            fragment

        } else {
            val fragment = VideosListFragment()
            val bundle = Bundle()
            bundle.putSerializable(
                BundleConstants.BUNDLE_THUMBNAIL_URL,
                thumbnailUrlList
            )
            bundle.putSerializable(
                BundleConstants.BUNDLE_PHOTOGRAPHER_NAME,
                videographerNameList
            )
            fragment.arguments = bundle
            fragment
        }
    }
}