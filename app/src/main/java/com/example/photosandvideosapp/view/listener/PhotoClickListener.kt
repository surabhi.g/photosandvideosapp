package com.example.photosandvideosapp.view.listener


/**
 * Created by Surabhi on 13,February,2021
 */

interface PhotoClickListener {

    fun onPhotoClick(
        photoUrl :String,
        photographerName:String
    )

}