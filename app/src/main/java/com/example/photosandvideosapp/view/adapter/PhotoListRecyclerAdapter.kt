package com.example.photosandvideosapp.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.photosandvideosapp.R
import com.example.photosandvideosapp.databinding.ItemPhotoListBinding
import com.example.photosandvideosapp.view.base.BaseViewHolder
import com.example.photosandvideosapp.view.listener.PhotoClickListener
import com.example.photosandvideosapp.view.viewholder.PhotoListViewHolder


/**
 * Created by Surabhi on 13,February,2021
 */

class PhotoListRecyclerAdapter(
    private val photoUrlList: ArrayList<String>,
    private val photographerNameList: ArrayList<String>,
    private val photoClickListener: PhotoClickListener
) : RecyclerView.Adapter<BaseViewHolder<*>>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val binding: ItemPhotoListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_photo_list, parent, false
        )

        return PhotoListViewHolder(
            binding,
            parent.context,
            photoUrlList,
            photographerNameList,
            photoClickListener
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is PhotoListViewHolder -> holder.bind(
                photoUrlList,
                position
            )
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun getItemCount(): Int = photoUrlList.size
}