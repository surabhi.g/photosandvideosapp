package com.example.photosandvideosapp.view.viewholder

import android.content.Context
import com.example.photosandvideosapp.R
import com.example.photosandvideosapp.databinding.ItemVideoListBinding
import com.example.photosandvideosapp.utils.GlideImageUtil
import com.example.photosandvideosapp.view.base.BaseViewHolder


/**
 * Created by Surabhi on 13,February,2021
 */

class VideoListViewHolder(
    private val binding: ItemVideoListBinding,
    private val context: Context,
    private val photoUrlList: ArrayList<String>,
    private val photographerNameList: ArrayList<String>
) : BaseViewHolder<Any>(binding.root) {

    override fun bind(item: Any?, position: Int) {
        binding.viewHolder = this

        GlideImageUtil.loadImage(
            context = context,
            imageUrl = photoUrlList[position],
            icPlaceholder = R.drawable.ic_launcher_background,
            icError = R.drawable.ic_launcher_background,
            imageView = binding.image
        )

        binding.photographerName.text = photographerNameList[position]
    }
}