package com.example.photosandvideosapp.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.photosandvideosapp.R
import com.example.photosandvideosapp.databinding.ItemVideoListBinding
import com.example.photosandvideosapp.view.base.BaseViewHolder
import com.example.photosandvideosapp.view.viewholder.VideoListViewHolder


/**
 * Created by Surabhi on 13,February,2021
 */

class VideoListAdapter(
    private val photoUrlList: ArrayList<String>,
    private val photographerNameList: ArrayList<String>
) : RecyclerView.Adapter<BaseViewHolder<*>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val binding: ItemVideoListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_video_list, parent, false
        )

        return VideoListViewHolder(
            binding,
            parent.context,
            photoUrlList,
            photographerNameList,
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is VideoListViewHolder -> holder.bind(
                photoUrlList,
                position
            )
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun getItemCount(): Int = photoUrlList.size
}
