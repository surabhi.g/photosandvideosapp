package com.example.photosandvideosapp.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.photosandvideosapp.R
import com.example.photosandvideosapp.application.constants.BundleConstants
import com.example.photosandvideosapp.databinding.FragmentPhotoListBinding
import com.example.photosandvideosapp.view.adapter.PhotoListRecyclerAdapter
import com.example.photosandvideosapp.view.listener.PhotoClickListener


/**
 * Created by Surabhi on 13,February,2021
 */

class PhotoListFragment :Fragment(),PhotoClickListener {

    private lateinit var binding: FragmentPhotoListBinding
    private var photoUrlList:ArrayList<String> = ArrayList()
    private var photographerNameList:ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            photoUrlList = it.getSerializable(BundleConstants.BUNDLE_PHOTO_URL) as ArrayList<String>
            photographerNameList = it.getSerializable(BundleConstants.BUNDLE_PHOTOGRAPHER_NAME) as ArrayList<String>
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
         binding = DataBindingUtil.inflate(inflater, R.layout.fragment_photo_list, container, false)
         return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.fragment = this

        binding.requestListRecyclerview.adapter = PhotoListRecyclerAdapter(photoUrlList,photographerNameList, this)
    }

    override fun onPhotoClick(photoUrl: String, photographerName: String) {
        //move to photo detail frag
        val fragment = PhotoDetailFragment()
        val bundle = Bundle()
        bundle.putString(
            BundleConstants.BUNDLE_PHOTO_URL,
            photoUrl
        )
        bundle.putString(
            BundleConstants.BUNDLE_PHOTOGRAPHER_NAME,
            photographerName
        )
        fragment.arguments = bundle
        val fm: FragmentManager = requireActivity().supportFragmentManager
        val transaction: FragmentTransaction = fm.beginTransaction()
        transaction.add(R.id.layout_activity, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

}