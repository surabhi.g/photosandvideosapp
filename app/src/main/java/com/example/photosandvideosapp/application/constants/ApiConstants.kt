package com.example.photosandvideosapp.application.constants


/**
 * Created by Surabhi on 13,February,2021
 */

object ApiConstants {

    const val BASE_URL ="https://api.pexels.com/"

    const val SERVER_ERROR = "Server Error"
    const val TIME_OUT_ERROR = "timeout"
    const val CONNECTION_TIME_OUT_ERROR = "Connection time out error"
}