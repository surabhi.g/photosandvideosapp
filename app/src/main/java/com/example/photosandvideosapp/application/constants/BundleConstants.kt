package com.example.photosandvideosapp.application.constants


/**
 * Created by Surabhi on 13,February,2021
 */

object BundleConstants {

    const val BUNDLE_PHOTO_URL = "bundle_photo_url"
    const val BUNDLE_THUMBNAIL_URL = "bundle_thumbnail_url"
    const val BUNDLE_PHOTOGRAPHER_NAME = "bundle_photographer_name"
}