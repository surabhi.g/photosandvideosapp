package com.example.photosandvideosapp.network.api

import com.example.photosandvideosapp.network.model.response.FetchPhotoListResponseModel
import com.example.photosandvideosapp.network.model.response.FetchVideoListResponseModel
import retrofit2.Call
import retrofit2.http.*

interface RetrofitApiService {

    @Headers("Authorization: 563492ad6f91700001000001ade308310cac42a3ace6394d485fd363")
    @GET("v1/curated")
    fun fetchCuratedPhoto(
        @Query("per_page") per_page: Int
    ): Call<FetchPhotoListResponseModel>?

    @Headers("Authorization: 563492ad6f91700001000001ade308310cac42a3ace6394d485fd363")
    @GET("v1/search")
    fun fetchPhotosList(
        @Query("query") query: String
    ): Call<FetchPhotoListResponseModel>?

    @Headers("Authorization: 563492ad6f91700001000001ade308310cac42a3ace6394d485fd363")
    @GET("videos/search")
    fun fetchVideosList(
        @Query("query") query: String
    ): Call<FetchVideoListResponseModel>?

}
