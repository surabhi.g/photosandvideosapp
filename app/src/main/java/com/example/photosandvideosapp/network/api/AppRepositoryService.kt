package com.example.photosandvideosapp.network.api

import com.example.photosandvideosapp.network.model.base.NetworkErrorResponse
import com.example.photosandvideosapp.network.model.response.FetchPhotoListResponseModel
import com.example.photosandvideosapp.network.model.response.FetchVideoListResponseModel
import retrofit2.Call


/**
 * Created by Surabhi on 13,February,2021
 */

class AppRepositoryService(private val repository: AppRepository = AppRepository())  {

    fun fetchPhotosListAPIRequest(
        call: Call<FetchPhotoListResponseModel>?,
        success: (FetchPhotoListResponseModel) -> Unit,
        error: (NetworkErrorResponse) -> Unit
    ) {
        repository.fetchPhotosList(call, success, error)
    }

    fun fetchVideosListAPIRequest(
        call: Call<FetchVideoListResponseModel>?,
        success: (FetchVideoListResponseModel) -> Unit,
        error: (NetworkErrorResponse) -> Unit
    ) {
        repository.fetchVideosList(call, success, error)
    }
}