package com.example.photosandvideosapp.network.model.response

import com.example.photosandvideosapp.network.model.base.BaseResponse

data class FetchVideoListResponseModel(
    val page: Int,
    val per_page: Int,
    val total_results: Int,
    val url: String,
    val videos: ArrayList<Video>
):BaseResponse()

data class User(
    val id: Int,
    val name: String,
    val url: String
):BaseResponse()

data class VideoFile(
    val file_type: String,
    val height: Int,
    val id: Int,
    val link: String,
    val quality: String,
    val width: Int
):BaseResponse()

data class Video(
    val avg_color: Any,
    val duration: Int,
    val full_res: Any,
    val height: Int,
    val id: Int,
    val image: String,
    val tags: ArrayList<Any>,
    val url: String,
    val user: User,
    val video_files: ArrayList<VideoFile>,
    val video_pictures: ArrayList<VideoPicture>,
    val width: Int
):BaseResponse()

data class VideoPicture(
    val id: Int,
    val nr: Int,
    val picture: String
):BaseResponse()