package com.example.photosandvideosapp.network.api

import com.example.photosandvideosapp.network.model.base.NetworkErrorResponse
import com.example.photosandvideosapp.network.model.response.FetchPhotoListResponseModel
import com.example.photosandvideosapp.network.model.response.FetchVideoListResponseModel
import retrofit2.Call


/**
 * Created by Surabhi on 13,February,2021
 */

class AppRepository(private val appRequestServer: AppRequestServer = AppRequestServer())
    : AppRepositoryInterface {

    override fun fetchPhotosList(
        call: Call<FetchPhotoListResponseModel>?,
        success: (FetchPhotoListResponseModel) -> Unit,
        error: (NetworkErrorResponse) -> Unit
    ) {
        appRequestServer.requestServer(call, success, error)
    }

    override fun fetchVideosList(
        call: Call<FetchVideoListResponseModel>?,
        success: (FetchVideoListResponseModel) -> Unit,
        error: (NetworkErrorResponse) -> Unit
    ) {
        appRequestServer.requestServer(call, success, error)

    }
}