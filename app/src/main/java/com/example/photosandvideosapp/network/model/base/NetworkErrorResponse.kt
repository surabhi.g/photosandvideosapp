package com.example.photosandvideosapp.network.model.base


data class NetworkErrorResponse(var message: String?, var details: Throwable?)