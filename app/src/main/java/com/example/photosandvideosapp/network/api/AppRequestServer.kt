package com.example.photosandvideosapp.network.api

import android.util.Log
import com.example.photosandvideosapp.application.constants.ApiConstants
import com.example.photosandvideosapp.network.model.base.NetworkErrorResponse
import retrofit2.Call
import java.util.*

class AppRequestServer {

    private val TAG = "AppRequestServer"

    fun <T> requestServer(
        call: Call<T>?,
        success: ((T) -> Unit),
        error: (NetworkErrorResponse) -> Unit
    ) {
        call?.enqueue(object :
            retrofit2.Callback<T> {

            override fun onFailure(call: Call<T>, errorDetails: Throwable) {
                val errorResponse: NetworkErrorResponse =
                    when (errorDetails.message?.toLowerCase(Locale.ROOT)) {
                        ApiConstants.TIME_OUT_ERROR ->
                            NetworkErrorResponse(
                                ApiConstants.CONNECTION_TIME_OUT_ERROR,
                                errorDetails.cause
                            )

                        else ->
                            NetworkErrorResponse(
                                errorDetails.message,
                                errorDetails.cause
                            )

                    }
                error.invoke(errorResponse)
                Log.d(TAG, "API: onFailure() $errorResponse")
            }

            override fun onResponse(
                call: Call<T>,
                response: retrofit2.Response<T>
            ) {
                response.body()?.let {
                    try {
                        success.invoke(it)
                    } catch (e: Exception) {
                        error.invoke(NetworkErrorResponse(ApiConstants.SERVER_ERROR, null))
                    }
                } ?: kotlin.run {
                    error.invoke(NetworkErrorResponse(ApiConstants.SERVER_ERROR, null))
                }
                Log.d(TAG, "API: onResponse() ${response.body()}")
            }
        })
    }
}