package com.example.photosandvideosapp.network.model.response

import com.example.photosandvideosapp.network.model.base.BaseResponse

data class FetchPhotoListResponseModel(
    val next_page: String,
    val page: Int,
    val per_page: Int,
    val photos: ArrayList<Photo>,
    val total_results: Int
):BaseResponse()

data class Photo(
    val avg_color: String,
    val height: Int,
    val id: Int,
    val liked: Boolean,
    val photographer: String,
    val photographer_id: Int,
    val photographer_url: String,
    val src: Src,
    val url: String,
    val width: Int
):BaseResponse()

data class Src(
    val landscape: String,
    val large: String,
    val large2x: String,
    val medium: String,
    val original: String,
    val portrait: String,
    val small: String,
    val tiny: String
):BaseResponse()