package com.example.photosandvideosapp.utils

import android.content.Context
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide


object GlideImageUtil {

    fun loadImage(
        context: Context?,
        imageUrl: String?,
        icPlaceholder: Int,
        icError: Int,
        imageView: AppCompatImageView
    ) {

        Glide
            .with(context!!)
            .load(imageUrl)
            .centerCrop()
            .placeholder(icPlaceholder)
            .error(icError)
            .into(imageView)

    }
}