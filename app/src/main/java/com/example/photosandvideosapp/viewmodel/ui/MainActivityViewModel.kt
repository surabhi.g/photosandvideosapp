package com.example.photosandvideosapp.viewmodel.ui

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.photosandvideosapp.application.constants.AppConstants
import com.example.photosandvideosapp.network.model.response.FetchPhotoListResponseModel
import com.example.photosandvideosapp.network.model.response.FetchVideoListResponseModel
import com.example.photosandvideosapp.viewmodel.base.BaseViewModel


/**
 * Created by Surabhi on 13,February,2021
 */

class MainActivityViewModel(application: Application) : BaseViewModel(application) {

    private val TAG = "MainActivityViewModel"

    var fetchCuratedPhotoResponse: MutableLiveData<FetchPhotoListResponseModel> = MutableLiveData()
    var fetchPhotoListResponse: MutableLiveData<FetchPhotoListResponseModel> = MutableLiveData()
    var fetchVideoListResponse: MutableLiveData<FetchVideoListResponseModel> = MutableLiveData()

    fun getHomeBannerImage() {
        repositoryService?.fetchPhotosListAPIRequest(
            retrofitApiService?.fetchCuratedPhoto(
                AppConstants.CONSTANT_1
            ), success = {
                fetchCuratedPhotoResponse.value = it
            }, error = {
                Log.d(TAG, "error =$it")
            })
    }

    fun fetchPhotosList() {
            repositoryService?.fetchPhotosListAPIRequest(
                retrofitApiService?.fetchPhotosList(
                    AppConstants.ANIMAL
                ), success = {
                    fetchPhotoListResponse.value = it
                }, error = {
                    Log.d(TAG, "error =$it")
                })
    }

    fun fetchVideosList() {
        repositoryService?.fetchVideosListAPIRequest(
            retrofitApiService?.fetchVideosList(
                AppConstants.ANIMAL
            ), success = {
                fetchVideoListResponse.value = it
            }, error = {
                Log.d(TAG, "error =$it")
            })
    }


}